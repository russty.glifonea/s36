const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})
	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result 
	})
}

// Update Task Controller
module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if (error) {
			console.log(error)
			return error
		}

		result.name = new_data.name 

		return result.save().then((updatedTask, error) => {
			if (error) {
				console.log(error)
				return error
			}
			
			return updatedTask
		})
	})
}



// Mini Activity - Delete Task
module.exports.deleteTask = (task_id, new_data) => {
	return Task.findByIdAndDelete(task_id).then((removedTask, error) => {
		if (error) {
			console.log(error)
			return error
		}

			return removedTask
	})
}

// Activity

module.exports.getSpecificTask = (task_id) => {
	return Task.findById(task_id).then((result) => {
			return result 
	})
}

module.exports.completeTask = (task_id, new_data) => {
	return Task.findById(task_id).then((completedTask, error) => {
		if (error) {
			console.log(error)
			return error
		}
	
			return completedTask
		})
}
