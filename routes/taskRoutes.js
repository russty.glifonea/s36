const express = require('express')
const router = express.Router()
const TaskController = require('../controller/TaskController')


//Create single task
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

//Miniactivity (30mins)
// Delete task
/*
	1. Set up a route for deleting a task
	2. Setup the controller for deleting a task
	3. Build teh controller function responsible for deleting a task
	4. Send the result of that controller function as a response

	Endpoint: '/:id/delete'
*/


// Delete a task
router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id, request.body).then((result) => {
		response.send('Successfully Deleted!')
	})
})

//Activity
router.get('/:id', (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result) => { 
		response.send(result)
	})
})


// Update a task
router.patch('/:id/complete', (request, response) => {
	TaskController.completeTask(request.params.status, request.body).then((result) => {
		response.send(result)
	})
})



module.exports = router


